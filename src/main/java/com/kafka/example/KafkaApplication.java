package com.kafka.example;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.kafka.core.KafkaTemplate;

@SpringBootApplication
public class KafkaApplication {

    public static void main(String[] args) {
        SpringApplication.run(KafkaApplication.class, args);
    }

    @Bean
    CommandLineRunner commandLineRunner(KafkaTemplate<String, String> kafkaTemplate) {
        return args -> kafkaTemplate.send("t-example", "hello kafka");
    }
}

/*

docker exec broker kafka-console-consumer --bootstrap-server broker:9092 --topic t-example --from-beginning

docker exec broker kafka-topics --bootstrap-server broker:9092 --describe --topic t-example
docker exec broker kafka-topics --bootstrap-server broker:9092 --list

*/