package com.kafka.example.consumers;

import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

@Component
public class KafkaConsumer {

    @KafkaListener(topics = "t-example", groupId = "groupId")
    void listener(String data) {
        System.out.println("Message received " + data);
    }
}
